package gob.sat.accuse.reporte;

import java.util.ArrayList;


public class LoadAccuse {
	
	

	
	
	public static ArrayList<AccuseBean> load(String dato1, String dato2){
		
		ArrayList<AccuseBean> list = new ArrayList<AccuseBean>();
		list.add(produce(dato1,dato2));
		
		return list;
		
	}
	
	
	  private static AccuseBean produce(String rfc, String accuse) {
		  AccuseBean dataBean = new AccuseBean();
	      dataBean.setRFC_CURP(rfc);
	      dataBean.setMENSAJE(accuse);
	      
	      return dataBean;
	   
	  }
	

	
	
}
