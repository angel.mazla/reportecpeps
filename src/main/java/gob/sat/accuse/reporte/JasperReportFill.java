package gob.sat.accuse.reporte;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class JasperReportFill {
	
	public static String inputStream="/Users/angel/proyectosSAT/reporte.pdf" ;
	
	 @SuppressWarnings("unchecked")
	 public static void getreporte() {
		 String sourceFileName = "/Users/angel/proyectosSAT/shcp-sat/generaaccuse/src/main/java/gob/sat/accuse/reporte/jasper_report_template.jasper";

		 	  LoadAccuse DataBeanList = new LoadAccuse();
		      ArrayList<AccuseBean> dataList = LoadAccuse.load("Dato1", "Dato2");

		      JRBeanCollectionDataSource beanColDataSource =
		      new JRBeanCollectionDataSource(dataList);

		      Map parameters = new HashMap();
		      /**
		       * Passing ReportTitle and Author as parameters
		       */
		      parameters.put("ReportTitle", "List of Contacts");
		      parameters.put("Author", "Prepared By Manisha");

		      try {
		         JasperFillManager.fillReportToFile(
		         sourceFileName, parameters, beanColDataSource);
		         JasperPrint jasperPrint = JasperFillManager.fillReport(JasperCompileManager.compileReport(inputStream), parameters, beanColDataSource); byte[] pdfBytes = JasperExportManager.exportReportToPdf(jasperPrint);
		      } catch (JRException e) {
		         e.printStackTrace();
		      }

	 } 
		      
}
