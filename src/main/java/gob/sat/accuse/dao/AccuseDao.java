package gob.sat.accuse.dao;

import java.util.List;

import gob.sat.accuse.modelo.SatAccuse;
import gob.sat.acusse.dao.exception.DaoException;

public interface AccuseDao {

	/**
	 * Metodo para generar la consulta.
	 * @throws DaoException 
	 */



	List<SatAccuse> obtenerconsulta(SatAccuse filtro) throws DaoException;

}