package gob.sat.accuse.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import gob.sat.accuse.modelo.SatAccuse;
import gob.sat.acusse.dao.exception.DaoException;




@Named("ConsultaBitacoraDaoJpa")
@Singleton
public class AccuseDaoJpaImpl implements AccuseDao {

 private static final String ERROR_SQL = "SQLException en el metodo generaConsulta() de la clase EmpleadoDAO: ";
	
	@Resource(lookup="java:/AGSPepshcpDS")
	private DataSource ds;
	
	private static final Logger ERROL = Logger.getLogger(AccuseDaoJpaImpl.class);
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gob.sat.poc.dao.EmpleadoDao#obtenerEmpleados(gob.sat.poc.modelo.Empleado)
	 */
	@Override
	public List<SatAccuse> obtenerconsulta(SatAccuse filtro) throws DaoException {
		Connection cnn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		LinkedList<SatAccuse> resultado = null;
		
		try {
			cnn = this.ds.getConnection();
			st = cnn.prepareStatement(this.querySelect(filtro));
			rs = st.executeQuery();

			resultado = new LinkedList<>();	
			String rfc= filtro.getCPEP();
			
			while (rs.next()) {
				
				SatAccuse consulta = new SatAccuse();
				
				consulta.setACCUSE(rs.getString("ACCUSE"));
				consulta.setCPEP(rfc);
				
			
				resultado.add(consulta);			
			}
						
		}catch(Exception sqle) {
	   DaoException dex = new DaoException("SQLException en el metodo generaConsulta() de la clase EmpleadoDAOJpaImpl: ",
				sqle);
		
		throw dex;
		}finally {
			if (rs != null) {
				ERROL.debug("Cerrando Resultset");
				try {
					rs.close();
				} catch (SQLException sqle) {
					ERROL.error( ERROR_SQL + sqle.getMessage());
				}
			}
			if (st != null) {
				ERROL.debug("Cerrando Statement");
				try {
					st.close();
				} catch (SQLException sqle) {
					ERROL.error( ERROR_SQL + sqle.getMessage());
				}
			}
			if (cnn != null) {
				ERROL.debug("Cerrando Conexion");
				try {
					cnn.close();
				} catch (SQLException sqle) {
					ERROL.error( ERROR_SQL + sqle.getMessage());
				}
			}
		}
		
		return resultado;
	}
	
	
	private String querySelect(SatAccuse findBitacora){
		StringBuilder query = new StringBuilder("SELECT ACCUSE FROM SAT_AGS_ACCUSE_SHCP_TBL ");
		query.append(" WHERE 1 = 1 ");
		
		ERROL.info("ESTE ES EL QUERY: "+ query.toString());
		
		return query.toString();
	}
	
	
		
		
	public DataSource getDs() {
		return ds;
	}


	public void setDs(DataSource ds) {
		this.ds = ds;
	}


}
