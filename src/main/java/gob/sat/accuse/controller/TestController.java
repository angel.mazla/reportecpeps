package gob.sat.accuse.controller;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import com.google.gson.Gson;


import javax.ws.rs.core.Response;

import gob.sat.accuse.dao.AccuseDao;
import gob.sat.accuse.modelo.SatAccuse;
import gob.sat.accuse.reporte.JasperReportFill;
import gob.sat.acusse.controller.exception.AgsServicioException;
import gob.sat.acusse.dao.exception.DaoException;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;


import org.apache.log4j.Logger;



@Path("/rest/generaaccuse")
public class TestController {

	private static final Logger LOINF = Logger.getLogger(TestController.class);
	
	
	@Inject
	private AccuseDao dao;

	@POST
	@Path("")
	@Produces(APPLICATION_JSON)
	public Response  getConsulta(String datos) throws AgsServicioException{
		Response response = null;
		List<SatAccuse> respuesta = null;
		
		
		String json = datos;    
		Gson gson = new Gson(); 
	
		LOINF.info("mensaje de entrada:"+ datos);
		SatAccuse persona= gson.fromJson(json, SatAccuse.class);
		
	
		
		try{
			
			respuesta = this.dao.obtenerconsulta(persona);
			JasperReportFill.getreporte();
			
			
		}catch(DaoException dex){
			throw new AgsServicioException(dex);
		}
		response = Response.status(200).entity(respuesta).build();
		LOINF.info("informacion que regresa"+response );
		return response;
		
	}
	

	


	
	
	
    public AccuseDao getDao() {
		return dao;
	}

	public void setDao(AccuseDao dao) {
		this.dao = dao;
	}

}
