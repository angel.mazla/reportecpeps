package gob.sat.acusse.controller.exception;

public class AgsServicioException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public AgsServicioException(){
		super();
	}
	
	public AgsServicioException(Throwable t){
		super(t);
	}
	
	public AgsServicioException(String message){
		super(message);
	}
	
	public AgsServicioException(String message, Throwable t){
		super(message, t);
	}

}
