package gob.sat.acusse.dao.exception;

public class DaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9032919194455994418L;
	public DaoException(){
		super();
	}
	
	public DaoException(Throwable t){
		super(t);
	}
	
	public DaoException(String message){
		super(message);
	}
	
	public DaoException(String message, Throwable t){
		super(message, t);
	}

}
